################
# ec2 instance #
################

resource "aws_instance" "this" {
  count = var.instance_count

  ami           = var.ami
  instance_type = var.instance_type

  subnet_id                            = element(var.subnet_ids, count.index)
  private_ip                           = length(var.private_ips) > 0 ? element(var.private_ips, count.index) : null
  iam_instance_profile                 = var.iam_instance_profile
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  disable_api_termination              = var.disable_api_termination
  monitoring                           = var.monitoring

  user_data        = var.user_data
  user_data_base64 = var.user_data_base64

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
      kms_key_id            = lookup(root_block_device.value, "kms_key_id", null)
    }
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      device_name           = ebs_block_device.value.device_name
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
      iops                  = lookup(ebs_block_device.value, "iops", null)
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(ebs_block_device.value, "encrypted", null)
      kms_key_id            = lookup(ebs_block_device.value, "kms_key_id", null)
    }
  }

  vpc_security_group_ids = var.vpc_security_group_ids
  key_name               = var.key_name

  tags = merge(
    {
      "Name" = format("%s-${format("%02d", count.index + 1)}", var.instance_name)
    },
    var.tags
  )

  volume_tags = merge(
    {
      "Name" = format("%s-${format("%02d", count.index + 1)}", var.instance_name)
    },
    var.volume_tags
  )
}
