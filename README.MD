# terraform-aws-ec2-instance

Terraform module to create and/or manage the below listed resources in AWS:

* EC2 instance(s)

## Repository Structure

```
├─ terraform-aws-ec2-instance
    ├── .gitignore
    ├── CHANGELOG.MD
    ├── main.tf
    ├── outputs.tf
    ├── README.MD
    └── variables.tf
```

*.gitignore* - exclude local `.terraform` directories and `.tfstate` files from git commit

*CHANGELOG.MD* - keep track of releases based on new features, enhancements and bug fixes

*main.tf* - main configuration file to create resources

*outputs.tf* - contains output definitions from resources created in `main.tf`

*README.MD* - document information about purpose and usage of the module

*variables.tf* - contains variable definitions used in `main.tf`

## Required Inputs

| Name | Description | Type |
| --- | --- | :---: |
| instance_name | instance name used as prefix | string |
| instance_count | number of instances | number |
| ami | ID of the AMI | string |
| instance_type | type of the instance | string |
| subnet_ids | list of VPC subnet IDs | list(string) |

## Optional Inputs

| Name | Description | Type | Default |
| --- | --- | :---: | :---: |
| private_ips | list of private IP addresses | list(string) |  `[]` |
| iam_instance_profile | name of the EC2 instance profile | string |  `""` | 
| instance_initiated_shutdown_behavior | shutdown behavior for the instance | string |  `""` |
| disable_api_termination | flag to enable/disable EC2 instance termination protection | bool |  `false` |
| monitoring | flag to enable/disable detailed monitoring | bool |  `false` |
| user_data | pass user data | list(string) | `null` |
| user_data_base64 | pass user data base64 encoded (instead of `user_data`) | list(string) | `null` |
| root_block_device | customize details about the root block device | list(map(string)) | `[]` |
| ebs_block_device | additional EBS block devices | list(map(string)) | `[]` |
| vpc_security_group_ids | list of security group IDs | list(string) | `null` |
| key_name | instance key pair name | string | `""` |
| tags | project specific tags | map(string) | `{}` |
| volume_tags | resource specific tags | map(string) | `{}` |

## Outputs

| Name | Description |
| --- | --- |
| instance_ids | IDs of the EC2 instances |
| instance_private_ips | private IP addresses of the EC2 instances |

## Example

Usage with *.auto.tfvars file and minimal required inputs.

```hcl
module "ec2" {
  source  = "app.terraform.io/cw_terraform_cloud/ec2/aws"
  version = "1.0.0"
  
  instance_name  = var.instance_name
  instance_count = var.instance_count

  ami           = var.ami
  instance_type = var.instance_type
  
  subnet_ids = var.subnet_ids

  root_block_device = var.root_block_device
  
  tags = var.tags
}
```

Usage with direct variable values and minimal required inputs.

```hcl
module "ec2" {
  source  = "app.terraform.io/cw_terraform_cloud/ec2/aws"
  version = "1.0.0"

  instance_name  = "EC2-FIGHTCLUB"
  instance_count = 1

  ami           = "ami-043097594a7df80ec"
  instance_type = "t2.micro"

  subnet_ids = ["subnet-a60abeda"]

  root_block_device = [
    {
      volume_size           = "20"
      volume_type           = "gp2"
      delete_on_termination = "true"
    }
  ]

  key_name = "fightclub-demo-kp"

  tags = {
    fightclub = "true"
  }
}
```
