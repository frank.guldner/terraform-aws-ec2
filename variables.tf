variable "instance_name" {
  type    = string
  default = ""
}

variable "instance_count" {
  type    = number
  default = 0
}

variable "ami" {
  type    = string
  default = ""
}

variable "instance_type" {
  type    = string
  default = ""
}

variable "subnet_ids" {
  type    = list(string)
  default = []
}

variable "private_ips" {
  type    = list(string)
  default = []
}

variable "iam_instance_profile" {
  type    = string
  default = ""
}

variable "instance_initiated_shutdown_behavior" {
  type    = string
  default = ""
}

variable "disable_api_termination" {
  type    = bool
  default = false
}

variable "monitoring" {
  type    = bool
  default = false
}

variable "user_data" {
  type    = string
  default = null
}

variable "user_data_base64" {
  type    = string
  default = null
}

variable "root_block_device" {
  type    = list(map(string))
  default = []
}

variable "ebs_block_device" {
  type    = list(map(string))
  default = []
}

variable "vpc_security_group_ids" {
  type    = list(string)
  default = null
}

variable "key_name" {
  type    = string
  default = ""
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "volume_tags" {
  type    = map(string)
  default = {}
}
